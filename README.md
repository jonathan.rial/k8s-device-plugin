# NVIDIA device plugin for Kubernetes with device renaming enabled

The official NVIDIA device plugin for Kubernetes doesn't support resource renaming in MPS GPU sharing mode. This is due to an arbitrary decision by NVIDIA's product team (see [#353](https://github.com/NVIDIA/k8s-device-plugin/issues/353#issuecomment-1387359917) and [#143](https://github.com/NVIDIA/k8s-device-plugin/issues/143#issuecomment-1325017300)). However, it is possible to reactivate this feature, and this README explains how to do so.

## Reactivate device renaming

Open the `api/config/v1/config.go` file with your favorite editor and remove these following lines:

```diff
  func DisableResourceNamingInConfig(logger logger, config *Config) {
-     // Disable resource renaming through config.Resource
-     if len(config.Resources.GPUs) > 0 || len(config.Resources.MIGs) > 0 {
-         logger.Warning("Customizing the 'resources' field is not yet supported in the config. Ignoring...")
-     }
-     config.Resources.GPUs = nil
-     config.Resources.MIGs = nil
-
-     // Disable renaming / device selection in Sharing.TimeSlicing.Resources
-     config.Sharing.TimeSlicing.disableResoureRenaming(logger, "timeSlicing")
-     // Disable renaming / device selection in Sharing.MPS.Resources
-     config.Sharing.MPS.disableResoureRenaming(logger, "mps")
  }
```

## Build the container image

To build the container image, run the following command:

```shell
docker build \
  --tag registry.forge.hefr.ch/jonathan.rial/k8s-device-plugin:$(git describe --tags) \
  --build-arg GOLANG_VERSION="$(./hack/golang-version.sh)" \
  --build-arg VERSION="$(git describe --tags)" \
  --build-arg GIT_COMMIT="$(git rev-parse --short HEAD)" \
  -f deployments/container/Dockerfile.ubi8 \
  .
```

## Push the container image to the registry

To push the container image to the registry, run the following command:

```shell
docker push registry.forge.hefr.ch/jonathan.rial/k8s-device-plugin:$(git describe --tags)
```

